package com.plombeer;


import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.*;

import java.io.IOException;
import java.util.Iterator;

/**
 * Created by plombeer on 01.03.17.
 */
public class HadoopMain {


    public static class Map extends MapReduceBase implements Mapper<LongWritable, Text, IntWritable, Transaction> {

        public void map(LongWritable key, Text value, OutputCollector<IntWritable, Transaction> output, Reporter reporter) throws IOException {
            String line = value.toString();
            String parse[] = line.split(" ");
            if(parse.length == 3) {
                Transaction t = new Transaction(Integer.parseInt(parse[0]),
                            Long.parseLong(parse[1]), Long.parseLong(parse[2]));

                output.collect(new IntWritable(t.getId()), t);
            }
        }
    }

    public static class Reduce extends MapReduceBase implements Reducer<IntWritable, Transaction, IntWritable, LongWritable> {
        public void reduce(IntWritable key, Iterator<Transaction> values, OutputCollector<IntWritable, LongWritable> output, Reporter reporter) throws IOException {
            long sum = 0;
            while (values.hasNext()) {
                sum += values.next().getSum();
            }
            output.collect(key, new LongWritable(sum));
        }
    }

    public static void main(String[] args) throws Exception {
        JobConf conf = new JobConf(HadoopMain.class);
        conf.setJobName("Bank Tracker");

        conf.setOutputKeyClass(IntWritable.class);
        conf.setOutputValueClass(LongWritable.class);

        conf.setMapperClass(Map.class);

        conf.setMapOutputKeyClass(IntWritable.class);
        conf.setMapOutputValueClass(Transaction.class);

        conf.setReducerClass(Reduce.class);

        conf.setInputFormat(TextInputFormat.class);
        conf.setOutputFormat(TextOutputFormat.class);

        FileInputFormat.setInputPaths(conf, new Path(args[0]));
        FileOutputFormat.setOutputPath(conf, new Path(args[1]));

        JobClient.runJob(conf);
    }
}
