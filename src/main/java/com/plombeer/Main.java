package com.plombeer;


import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Random;

import static java.lang.System.out;

/**
 * Created by plombeer on 01.03.17.
 */
public class Main {

    public static void main(String[] args) throws IOException, URISyntaxException {
        Random rand = new Random();


        Configuration configuration = new Configuration();
        FileSystem hdfs = FileSystem.get(new URI("hdfs://master:9000"), configuration);
        Path file = new Path("hdfs://master:9000/my_storage/banks.txt");
        if (hdfs.exists(file)) {
            hdfs.delete(file, true);
        }
        OutputStream os = hdfs.create(file, () -> out.println("...bytes written: [ " + " ]"));
        BufferedWriter br = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));

        long date = System.currentTimeMillis();
        for (int i = 0; i < 1000000; i++) {
            br.write(String.valueOf(rand.nextInt(50)));
            br.write(" ");
            br.write(String.valueOf(rand.nextInt(500000)));
            br.write(" ");
            br.write(String.valueOf(date));
            br.newLine();

            date += rand.nextInt(50) * 1000;
        }

        br.close();
        hdfs.close();

    }
}
