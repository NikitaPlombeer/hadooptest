package com.plombeer;

import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class Transaction implements Writable {

        private int id;
        private long sum;
        private long date;

        public Transaction() {
        }


        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public long getSum() {
            return sum;
        }

        public void setSum(long sum) {
            this.sum = sum;
        }

        public long getDate() {
            return date;
        }

        public void setDate(long date) {
            this.date = date;
        }

        public Transaction(int id, long sum, long date) {
            this.id = id;
            this.sum = sum;
            this.date = date;
        }

        @Override
        public void write(DataOutput dataOutput) throws IOException {
            dataOutput.writeInt(id);
            dataOutput.writeLong(sum);
            dataOutput.writeLong(date);
        }

        @Override
        public void readFields(DataInput dataInput) throws IOException {
            id = dataInput.readInt();
            sum = dataInput.readLong();
            date = dataInput.readLong();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Transaction that = (Transaction) o;

            return id == that.id && sum == that.sum && date == that.date;
        }

        @Override
        public int hashCode() {
            int result = id;
            result = 31 * result + (int) (sum ^ (sum >>> 32));
            result = 31 * result + (int) (date ^ (date >>> 32));
            return result;
        }
    }